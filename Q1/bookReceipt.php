<?php
    //FORM DATA
    $fiction = $_POST['fiction'];
    $history = $_POST['history'];
    $novel = $_POST['novel'];
    $DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
    
    //BOOK PRICES
?>

<html>
    <head>
        <title>Book receipt</title>
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <style>
            #receipt{
                width:25%;
            }
        </style>
    </head>
    <body>
        <div id="receipt">
        <?php
        
   /*echo "<script>alert('{$DOCUMENT_ROOT}')</script>";*/
        
    define('fictionPrice', 10);
    define('historyPrice', 20);
    define('novelPrice', 30);
    
    $quantity = $fiction + $history + $novel;
    $price = fictionPrice * $fiction + historyPrice * $history + novelPrice * $novel; 
            echo "<h3>Number of books ordered: $quantity </h3><br/>";        
        echo '<ul class="list-group">';
  

if( $quantity == 0)
{
    echo "<br /><li class='list-group-item list-group-item-danger'>You did not order any books!</li>";
}
else
{
  if ( $fiction>0 )
    echo "<li class='list-group-item'><b>Fiction books: </b>$fiction</li>";
    
  if ( $history>0 )
    echo "<li class='list-group-item'><b>History books: </b>$history</li>"; 
  if ( $novel>0 )
     echo "<li class='list-group-item'><b>Novel books: </b>$novel</li>";
}
      echo "<li class='list-group-item'><b>Total price: </b>$$price</li>";

            $date = date('F jS, Y (H:i)');
echo "<li class='list-group-item list-group-item-success'><b>Order processed at: </b>$date</li>";
echo '</ul>';
       
       $outputStr = $date."\t".$fiction." fiction \t".$history." history\t"
                  .$novel." novel\t". $quantity." quantity\t".$price."\n";
                  
       @ $file = fopen("$DOCUMENT_ROOT/Q1/records/bookOrders.txt", 'ab');
       flock($file, LOCK_EX);
       fwrite($file, $outputStr, strlen($outputStr));
       flock($file, LOCK_UN);
       fclose($file);
       
       echo '<p>Your order has been recorded</p>'; 
        ?>
        </div>
    </body>
</html>
    
    
    
    