<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" media="screen" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/css/bootstrap.min.css" />

    <title>Minimum Bootstrap HTML Skeleton</title>
    <!--  -->
    <style></style>

</head>

<body>
    
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <form action="addPost.php" method="POST" role="form" enctype="multipart/form-data">
                <h3>Add a post</h3>

                <div class="form-group">
                    <label for="title">Title:</label>
                    <input type="text" class="form-control" name="title" placeholder="Title.." />
                </div>

                <div class="form-group">
                    <label for="content">Content:</label>
                    <input type="text" class="form-control" name="content" placeholder="Content.." />
                </div>

                <div class="form-group">
                    <label for="fileToUpload">Select file to upload:</label>
                    <input type="file" name="fileToUpload" />
                </div>

                <button type="submit" class="btn btn-primary">Add</button>
                
<?php
if($_SERVER['REQUEST_METHOD'] === 'POST'){
require('db.php');
$title = $_POST['title'];
$content = $_POST['content'];

if(!empty($title) && !empty($content)){
    session_start();
    $fileToUpload = $_FILES["fileToUpload"]["name"];
    $sql = "";
    if(isset($fileToUpload) && $fileToUpload != ""){
        $target_dir = "uploads/";
        $target_file = $target_dir . basename($fileToUpload);
        $uploadOk = 1;
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
        // Check if image file is a actual image or fake image
        if(isset($_POST["submit"])) {
            $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
            if($check !== false) {
                echo "File is an image - " . $check["mime"] . ".";
                $uploadOk = 1;
            } else {
                echo "File is not an image.";
                $uploadOk = 0;
            }
        }
        // Check if file already exists
        if (file_exists($target_file)) {
            echo "Sorry, file already exists.";
            $uploadOk = 0;
        }
        // Check file size
        if ($_FILES["fileToUpload"]["size"] > 500000) {
            echo "Sorry, your file is too large.";
            $uploadOk = 0;
        }
        // Allow certain file formats
        if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
        && $imageFileType != "gif" ) {
            echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
            $uploadOk = 0;
        }
        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            echo "Sorry, your file was not uploaded.";
        }
            // if everything is ok, try to upload file
         else {
            if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
                echo "The file ". basename($_FILES["fileToUpload"]["name"]). " has been uploaded.";
                $fileToUpload = $target_file;
                if($_SESSION["admin"] == 1){
                    $sql = "INSERT INTO posts (header, content, approved, photo) VALUES (?,?,1,'$fileToUpload');";
                }
                else{
                    $sql = "INSERT INTO posts (header, content, photo) VALUES (?,?,'$fileToUpload');";
                }
            } else {
                echo "Sorry, there was an error uploading your file.";
            }
        }
    }
    else {
        if($_SESSION["admin"] == 1){
            $sql = "INSERT INTO posts (header, content, approved) VALUES (?,?,1);";
        }
        else{
            $sql = "INSERT INTO posts (header, content) VALUES (?,?);";
        }
    }

    if($sql != ""){
        $prepStat = $conn->prepare($sql);
        $prepStat->bind_param("ss", $title, $content);
        $prepStat->execute();
        echo "<h6>Your new post has been submited</h6>";
        $prepStat->close();
    }
  
    $conn->close();
}
}
?>
            </form>
        </div>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/js/bootstrap.min.js"></script>

    <script></script>
</body>
</html>



