<?php 
    session_start();
      if (!(isset($_SESSION['id']))) {
          header ("Location: login.php");
      }
?>
<html lang="en">
<head>
    <title>Bootstrap Case</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

    <div class="container">
        <h2>All posts</h2>
        <div class="panel-group">
            <?php

            require('db.php');
            include_once 'userClass.php';
            if($_SERVER['REQUEST_METHOD'] === 'POST'){
                $postId = $_POST["postId"];

                $SQL = "UPDATE posts SET approved = 1 WHERE id = $postId ;";
                mysqli_query($conn, $SQL);
            }
            session_start();
            $username = $_SESSION['username'];
            $userId = $_SESSION['id'];
            $isAdmin = $_SESSION['admin'];

            if($isAdmin == "0"){
                $SQL = "SELECT * FROM posts WHERE approved = 1";
            }
            else{
                $SQL = "SELECT * FROM posts ORDER BY approved DESC";
            }
            
            $y = $SQL;
            $result = mysqli_query($conn, $SQL);
            $num_rows = $result->num_rows;

            if ($num_rows > 0) {
                while($row = $result->fetch_assoc()) {
                    extract($row);

                    echo "<div class=\"panel panel-default\">\n";
                    echo "      <div class=\"panel-heading\">$header</div>\n";
                    echo "      <div class=\"panel-body\">\n";
                    echo "            <div>$content</div><br/>\n";
                    if(isset($photo)){
                        echo "        <a href='$photo'><img src='$photo' height='100'></a><br/>\n";
                    }
                    if($isAdmin == "1" && $approved == "0"){
                        echo "<form action=\"index.php\" method=\"post\">\n";
                        echo "   <input type=\"hidden\" name=\"postId\" value=\"$id\" />\n";
                        echo "   <input style='float:right' class = 'btn btn-default' type=\"submit\" value=\"Approve\" />\n";
                        echo "</form>";
                    }                  
                    echo "      </div>\n";
                    echo "    </div>";
                }
            }
            ?>
        </div>
        <a class="btn btn-default" href="addPost.php">Add new post</a>     
        <div>
            <br />
            <a class="btn btn-warning" href="logout.php">Logout</a>
        </div>

    </div>

</body>
</html>
