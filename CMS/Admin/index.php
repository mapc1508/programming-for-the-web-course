<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" media="screen" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/css/bootstrap.min.css" />

    <title>Admin</title>

</head>

<body>

    <div class="container">
        <h2>All posts</h2>
        <div class="panel-group">
            <?php
            require('../db.php');

            if($_SERVER['REQUEST_METHOD'] === 'POST'){
                $postId = $_POST["postId"];

                $SQL = "UPDATE posts SET approved = 1 WHERE id = $postId ;";
                mysqli_query($conn, $SQL);          
            }
            
            $SQL = "SELECT * FROM posts WHERE approved = 0";

            $result = mysqli_query($conn, $SQL);
            $num_rows = $result->num_rows;

            if ($num_rows > 0) {
                while($row = $result->fetch_assoc()) {
                    extract($row);
                    echo "<div class=\"panel panel-default\">\n";
                    echo "      <div class=\"panel-heading\">$header</div>\n";
                    echo "      <div class=\"panel-body\">\n";
                    echo "            <div>$content</div><br/>\n";
                    echo "<form action=\"index.php\" method=\"post\">\n";
                    echo "                <input type=\"hidden\" name=\"postId\" value=\"$id\" />\n";
                    echo "                <input style='float:right' class = 'btn btn-default' type=\"submit\" value=\"Approve\" />\n";
                    echo "            </form>";
                    echo "      </div>\n";
                    echo "    </div>";
                }
            }

            $conn->close();

            ?>

        </div>
        <a class="btn btn-default" href="../addPost.php">Add new post</a>

    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/js/bootstrap.min.js"></script>

    <script></script>
</body>
</html>

<?php

?>