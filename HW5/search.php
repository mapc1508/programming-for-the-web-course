<?php
       
    function getSameFirstLetterWords($word, $sentence){
        $firstLetter = substr(strtolower($word),0,1);
        $words = explode(" ",$sentence);
        $resultArray = array();
        
        for($i=0; $i<count($words); $i++){
            $words[$i] = trim($words[$i], " ,.");       
            if(substr(strtolower($words[$i]),0,1) == $firstLetter){
                array_push($resultArray,$words[$i]);
            }
        }
        return $resultArray;
    }

    $word = $_POST["word"];
    $sentence = $_POST["sentence"];
    //print_r(getSameFirstLetterWords($word, $sentence)); 
    
    $result = getSameFirstLetterWords($word, $sentence);
    echo "<h3>Result:</h3>";
    echo "<ul>";
        for($i = 0; $i < count($result); $i++){
            echo "<li>".$result[$i]."</li>";
        }
    echo "</ul>";    
?>