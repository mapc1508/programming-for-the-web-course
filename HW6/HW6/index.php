<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Ice cream SHOP</title>

    <!-- Bootstrap -->
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
</head>

    
<body>
    <div class="container-fluid" style="width:50%; margin-left:auto; margin-right:auto; margin-top:20px;">
        <form action="index.php" method="post">
            <?php
        $username = "z2doc_mapc1508";
        $password = "Miki1806.";
        $server = "94.76.208.180";
        $database = "z2doc_mapc1508";
        $conn = new mysqli($server, $username, $password, $database);
        if($conn->connect_error){
            die("Connection failed: " . $conn->connect_error);
        }
        $sql = "SELECT * FROM Icecream;";
        $result = $conn->query($sql);
        echo "<div class=\"form-group\">\n";
        echo "<label for='flavor'>Select flavor:</label>";
        echo "<select name='flavor' class='form-control'>";
        if($result->num_rows > 0){
            for ($i = 0; $i < $result->num_rows; $i++)
            {
                $row = $result->fetch_assoc();/*fetch associative array (row by row) from the result*/
                echo "<option value='" . $row['Flavor'] . "'>" . $row['Flavor'] . "</option>";
            }
        }
        echo "</select>";
        echo "</div>";
            ?>
            <div class="form-group">
                <label class="control-label" for="scoops">Scoops: </label>
                <input type="number" min="0" class="form-control" name="scoops">
            </div>
            <input type="submit" value="Order" class="btn btn-primary" />
        </form>
        <?php
        $flavor = $_POST['flavor'];
        $scoops = $_POST['scoops'];

        $conn = new mysqli($server, $username, $password, $database);

        if($conn->connect_error){
            die("Connection failed: " . $conn->connect_error);
        }
        if(!empty($scoops)){

            PREPARED STATEMENT FOR SELECTING
            $stmt = $conn->prepare("SELECT Id, Quantity FROM Icecream WHERE Flavor = ?");
            $stmt->bind_param('s', $flavor);
            $stmt->execute();

            $stmt->store_result();
            $stmt->bind_result($Id, $Quantity);

            while($stmt->fetch())
            {
                echo "col1=$Id, col2=$Quantity \n";
            }

            $stmt->close();}
            $sql = "SELECT Id, Quantity FROM Icecream WHERE Flavor = '" . $flavor . "';";
            $result = $conn->query($sql);
            $row = $result->fetch_assoc();
            $IcecreamId = $row["Id"];
            $InStock = $row["Quantity"];

            $scoopAmount = 50;

            if($scoopAmount*$scoops <= $InStock){
                $sql = "INSERT INTO Orders(Icecream_Id, Scoops) VALUES (?,?);";
                $prepStat = $conn->prepare($sql);
                $prepStat->bind_param("ii", $IcecreamId, $scoops);
                $prepStat->execute();
                echo "<br/>You have successfully bought the desired ice cream!";
                $prepStat->close();

                $sql = "UPDATE Icecream SET Quantity = Quantity - " . ($scoops * 50) . ", Sold = " . ($scoops * 50) . " WHERE Id = " . $IcecreamId;
                $conn->query($sql);
            }
            else{
                echo "<br/>Not enough icecream of this flavor in the stock";
            }          
        }
        else{
            echo "<br/>*All fields are required";
        }
        $conn->close();
        ?>      
        </div>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</body>
</html>

