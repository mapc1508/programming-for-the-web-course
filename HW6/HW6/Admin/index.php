<!DOCTYPE html>
<html>
<head>
    <title>Icecream shop - ADMIN</title>

    <!-- Bootstrap -->
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
    <div class="container-fluid" style="width:50%; margin-left:auto; margin-right:auto; margin-top:20px;">
        <form action="index.php" method="POST" role="form">
            <div class="form-group">
                <label for="flavor">Add flavor: </label>
                <input type="text" class="form-control" name="flavor">
            </div>
            <div class="form-group">
                <label for="quantity">Add quantity: </label>
                <input type="number" class="form-control" name="quantity">
            </div>
            <input type="submit" value="Add" class="btn btn-success">
        </form>

<?php
$flavor = $_POST['flavor'];
$quantity = $_POST['quantity'];

$username = "z2doc_mapc1508";
$password = "Miki1806.";
$server = "localhost";
$database = "z2doc_mapc1508";

$conn = new mysqli($server, $username, $password, $database);

if($conn->connect_error){
    die("Connection failed: " . $conn->connect_error);
}
if(!empty($flavor) && !empty($quantity)){

    $sql = "INSERT INTO Icecream(Flavor, Quantity, Sold) VALUES (?,?,0);";

    $prepStat = $conn->prepare($sql);
    $prepStat->bind_param("sd", $flavor, $quantity);
    $prepStat->execute();
    $prepStat->close();
    $conn->close();

    echo "<br/>New flavor has been successfully added!";
}

?>

    </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</body>
</html>
