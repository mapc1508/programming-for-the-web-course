<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Stats</title>

    <!-- Bootstrap -->
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">

</head>
<body>
    <table class="table">
        <thead>
            <tr>
                <th>Flavor</th>
                <th>Average number of scoops ordered</th>
                <th>KG Sold</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <?php
                $username = "z2doc_mapc1508";
                $password = "Miki1806.";
                $server = "localhost";
                $database = "z2doc_mapc1508";

                $conn = new mysqli($server, $username, $password, $database);

                if($conn->connect_error){
                    die("Connection failed: " . $conn->connect_error);
                }

                $sql = "SELECT i.Flavor, avg(o.scoops) as AverageOrdered, i.Sold/1000 as KG_Sold From Orders as o, Icecream as i
        WHERE o.Icecream_Id = i.Id Group by Icecream_Id;";
                $result = $conn->query($sql);

                for ($i = 0; $i < $result->num_rows; $i++)
                {
                    $row = $result->fetch_assoc();
                    echo "<tr>";
                    echo "<td>" . $row["Flavor"]. "</td>";
                    echo "<td>" . number_format($row["AverageOrdered"],2) . "</td>";
                    echo "<td>" . number_format($row["KG_Sold"],3) . "</td>";
                    echo "</tr>";
                }
                $conn->close();
                ?>
        </tbody>
    </table>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</body>
</html>

