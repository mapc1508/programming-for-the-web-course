<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap 101 Template</title>

    <!-- Bootstrap -->
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
    <form action="formSQL.php" method="post">
        <div class="form-group">
            <label class="control-label" for="Name">Name</label>
            <input type="text" class="form-control" name="Name" placeholder="Name" />
        </div>
        <div class="form-group">
            <label class="control-label" for="LastName">Last name</label>
            <input type="text" class="form-control" name="LastName" placeholder="Last name" />
        </div>
        <div class="form-group">
            <label class="control-label" for="Email">Email</label>
            <input type="text" class="form-control" name="Email" placeholder="Email" />
        </div>
        <input type="submit" value="submit" class="btn btn-primary"/>
    </form>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</body>
</html>

