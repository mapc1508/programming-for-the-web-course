<?php
  $DOCUMENT_ROOT = $_SERVER['DOCUMENT_ROOT'];
?>
    <html>

    <head>
        <title>Bob's Auto Parts - Orders</title>
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/site.css">
    </head>

    <body>
        <?php
  $orders= file("$DOCUMENT_ROOT/HW3/orders/orders.txt");
  // count the number of orders in the array
  $number_of_orders = count($orders);
  if ($number_of_orders == 0)
  {
    echo '<p><strong>No orders for now.
            Try again later.</strong></p>';
  }
  echo '</table>';
echo "<div class=\"container\">\n";
echo "  <h2>Bob's Auto Parts - Orders</h2>\n";
echo "  <table class=\"table table-bordered\">\n";
echo "    <thead>\n";
echo "      <tr>\n";
echo "        <th>Order Date</th>\n";
echo "        <th>Tires</th>\n";
echo "        <th>Oil</th>\n";
echo "        <th>Spark</th>\n";
echo "        <th>Total</th>\n";
echo "        <th>Address</th>\n";
echo "      </tr>\n";
echo "    </thead>\n";
echo "    <tbody>\n";
  for ($i=0; $i<$number_of_orders; $i++)
  {
    $line = explode( "\t", $orders[$i] );
    $line[1] = intval( $line[1] );
    $line[2] = intval( $line[2] );
    $line[3] = intval( $line[3] );
    echo "<tr><td>$line[0]</td>
                <td>$line[1]</td>
                <td>$line[2]</td>
                <td>$line[3]</td>
                <td>$line[4]</td>
                <td>$line[5]</td>
            </tr>";
  }
    
    echo "    </tbody>\n";
echo "  </table>\n";
echo "</div>";
    
?>
    </body>

    </html>