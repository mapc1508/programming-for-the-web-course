<?php
echo "<nav class=\"navbar navbar-default\" role=\"navigation\">\n";
echo "    <div class=\"container-fluid\">\n";
echo "        <!-- Brand and toggle get grouped for better mobile display -->\n";
echo "        <div class=\"navbar-header\">\n";
echo "            <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-1\">\n";
echo "                <span class=\"sr-only\">Toggle navigation</span>\n";
echo "                <span class=\"icon-bar\"></span>\n";
echo "                <span class=\"icon-bar\"></span>\n";
echo "                <span class=\"icon-bar\"></span>\n";
echo "            </button>\n";
echo "            <a class=\"navbar-brand\" href=\"index.php\">Home</a>\n";
echo "        </div>\n";
echo "        <!-- Collect the nav links, forms, and other content for toggling -->\n";
echo "        <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-1\">\n";
echo "            <ul class=\"nav navbar-nav\">\n";
echo "                <li>\n";
echo "                    <a href=\"settings.php\">Settings</a>\n";
echo "                </li>\n";
echo "                <li>\n";
echo "                    <a href=\"manageSubjects.php\">Manage subjects</a>\n";
echo "                </li>\n";
echo "                <li>\n";
echo "                    <a href=\"manageStudents.php\">Manage students</a>\n";
echo "                </li>\n";
echo "                <li>\n";
echo "                    <a href=\"subjects.php\">List of subjects</a>\n";
echo "                </li>\n";
echo "                <li>\n";
echo "                    <a href=\"students.php\">List of students</a>\n";
echo "                </li>\n";
echo "                <li>\n";
echo "                    <a href=\"studentSubject.php\">Add student to subject</a>\n";
echo "                </li>\n";
echo "                <li>\n";
echo "                    <a href=\"statistics.php\">Student statistics</a>\n";
echo "                </li>\n";
echo "\n";
echo "            </ul>\n";
echo "\n";
echo "        </div>\n";
echo "        <!-- /.navbar-collapse -->\n";
echo "    </div>\n";
echo "    <!-- /.container-fluid -->\n";
echo "</nav>";


?>