<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>Stats</title>

    <!-- Bootstrap -->
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" />

</head>
<body>
    <?php include 'navbar.php';?>
    <table class="table">
        <thead>
            <tr>
                <th>Student</th>
                <th>ECTS Points</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <?php
                require('db.php');

                $sql = "SELECT s.Name as name, sum(sub.ects) as TotalECTS From students as s, subjects as sub, student_subject ss
        WHERE ss.student_id = s.Id AND ss.subject_id = sub.Id Group by s.ID;";
                $result = $conn->query($sql);

                for ($i = 0; $i < $result->num_rows; $i++)
                {
                    $row = $result->fetch_assoc();
                    echo "<tr>";
                    echo "<td>" . $row["name"]. "</td>";
                    echo "<td>" . $row["TotalECTS"] . "</td>";
                    echo "</tr>";
                }
                $conn->close();
                ?>
        </tbody>
    </table>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</body>
</html>

