<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>Bootstrap 101 Template</title>

    <!-- Bootstrap -->
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" />

</head>
<body>
    <?php include 'navbar.php';?>
    <div class="container-fluid" style="width:50%; margin-left:auto; margin-right:auto; margin-top:20px;">
        <form action="manageStudents.php" method="POST" role="form">
            <div class="form-group">
                <label for="student">Add student name: </label>
                <input type="text" class="form-control" name="student" />
            </div>
            <input type="submit" value="Add" class="btn btn-success" />
        </form>
        <?php
        require('db.php');
        $name = $_POST['student'];

        if(!empty($name)){

            $sql = "INSERT INTO students(name) VALUES (?);";

            $prepStat = $conn->prepare($sql);
            $prepStat->bind_param("s", $name);
            $prepStat->execute();
            $prepStat->close();
            $conn->close();

            echo "<br/>New student has been successfully added!";
        }

        $conn->close();
        ?>
    </div>
</body>
</html>
