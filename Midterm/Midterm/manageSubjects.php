<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>Bootstrap 101 Template</title>

    <!-- Bootstrap -->
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" />

</head>
<body>
    <div class="container-fluid" style="width:50%; margin-left:auto; margin-right:auto; margin-top:20px;">
        <form action="manageSubjects.php" method="POST" role="form">
            <div class="form-group">
                <label for="subject">Add subject: </label>
                <input type="text" class="form-control" name="subject" />
            </div>
            <div class="form-group">
                <label for="ects">Add ECTS point: </label>
                <input type="number" class="form-control" name="ects" />
            </div>
            <input type="submit" value="Add" class="btn btn-success" />
        </form>
        <?php
        require('db.php');
        $name = $_POST['subject'];
        $ects = $_POST['ects'];

        if(!empty($name) && !empty($ects)){

            $sql = "INSERT INTO subjects(name, ects) VALUES (?,?);";

            $prepStat = $conn->prepare($sql);
            $prepStat->bind_param("si", $name, $ects);
            $prepStat->execute();
            $prepStat->close();
            $conn->close();

            echo "<br/>New subject has been successfully added!";
        }
        $conn->close();
        ?>
        </div>
</body>
</html>
