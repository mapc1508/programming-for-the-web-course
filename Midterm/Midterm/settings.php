<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap 101 Template</title>

    <!-- Bootstrap -->
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
    <?php include 'navbar.php';?>
    <div class="container-fluid" style="width:50%; margin-left:auto;margin-right:auto;margin-top:20px;">
        <form action="settings.php" method="post">
            <div class="form-group">
                <label for="name">Professor's name:</label>
                <input type="text" name="name" class="form-control" required="required" />      
            </div>
            <button type="submit" class="btn btn-primary">Add</button>
        </form>

        <?php
        require('db.php');

        $professor = $_POST['name'];
        if(!empty($professor)){
            $stmt = $conn->prepare("UPDATE `professors` SET `Name`=? WHERE `ID`='1';");
            $stmt->bind_param('s', $professor);
            $stmt->execute();
            if(!empty($professor)){
                echo "<br/>Your name has been stored";
            }
            $stmt->close();
        }      
        $conn->close();
        ?>
    </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</body>
</html>

