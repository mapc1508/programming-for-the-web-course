<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>Stats</title>

    <!-- Bootstrap -->
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" />

</head>
<body>
    <?php include 'navbar.php';?>
    <table class="table" style="width:50%; margin-left:auto; margin-right:auto; margin-top:20px;">
        <thead>
            <tr>
                <th>Student</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <?php
                require('db.php');

                $sql = "SELECT Name FROM students ORDER BY Name;";
                $result = $conn->query($sql);

                for ($i = 0; $i < $result->num_rows; $i++)
                {
                    $row = $result->fetch_assoc();
                    echo "<tr>";
                    echo "<td>" . $row["Name"]. "</td>";
                    echo "</tr>";
                }
                $conn->close();
                ?>
        </tbody>
    </table>
</body>
</html>

