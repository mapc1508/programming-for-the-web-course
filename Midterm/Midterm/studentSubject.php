<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>Ice cream SHOP</title>

    <!-- Bootstrap -->
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" />
</head>


<body>
    <?php include 'navbar.php';?>
    <div class="container-fluid" style="width:50%; margin-left:auto; margin-right:auto; margin-top:20px;">
        <form action="studentSubject.php" method="post">
            <?php
            require('db.php');
            $sql = "SELECT * FROM students ORDER BY name;";
            $result = $conn->query($sql);
            echo "<div class=\"form-group\">\n";
            echo "<label for='studentName'>Select student:</label>";
            echo "<select name='studentName' class='form-control'>";
            if($result->num_rows > 0){
                for ($i = 0; $i < $result->num_rows; $i++)
                {
                    $row = $result->fetch_assoc();
                    echo "<option value='" . $row['name'] . "'>" . $row['name'] . "</option>";
                }
            }
            echo "</select>";
            echo "</div>";

            $sql = "SELECT * FROM subjects ORDER BY name;";
            $result = $conn->query($sql);
            echo "<div class=\"form-group\">\n";
            echo "<label for='subjectName'>Select subject:</label>";
            echo "<select name='subjectName' class='form-control'>";
            if($result->num_rows > 0){
                for ($i = 0; $i < $result->num_rows; $i++)
                {
                    $row = $result->fetch_assoc();
                    echo "<option value='" . $row['name'] . "'>" . $row['name'] . "</option>";
                }
            }
            echo "</select>";
            echo "</div>";
            echo  "<input type='submit' value='Submit' class='btn btn-primary' />";
            echo "<br/><br/>";

            $subject = $_POST['subjectName'];
            $student = $_POST['studentName'];

            if(!empty($subject) && !empty($student)){
                $sql = "SELECT Id FROM subjects WHERE name = '$subject';";
                $result = $conn->query($sql);
                $row = $result->fetch_assoc();
                $subjectId = $row["Id"];

                $sql = "SELECT Id FROM students WHERE name = '$student';";
                $result = $conn->query($sql);
                $row = $result->fetch_assoc();
                $studentId = $row["Id"];

                $sql = "INSERT INTO student_subject(student_id, subject_id) VALUES (?,?);";
                $prepStat = $conn->prepare($sql);
                $prepStat->bind_param("ii", $studentId, $subjectId);
                $prepStat->execute();
                echo "Student was successfully assigned to subject!<br/><br/>";
                $prepStat->close();
            }

            $sql = "SELECT students.name as Student, subjects.name as Subject
                    FROM subjects
                    JOIN student_subject
                    ON student_subject.subject_id = subjects.id
                    JOIN students
                    ON students.id = student_subject.student_id
                    ORDER BY Student";
            $result = $conn->query($sql);

            for ($i = 0; $i < $result->num_rows; $i++)
            {
                $row = $result->fetch_assoc();
                echo $row["Student"] . " -> " .  $row["Subject"] . "<br/>";
            }
            $conn->close();
            ?>

        </form>
    </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</body>
</html>

