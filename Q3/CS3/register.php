<?php
    require('db.php');
    $username = $_POST['username'];
    $password = $_POST['password'];

    if(!empty($username) && !empty($password)){

        $sql = "INSERT INTO Users(Username, Pass) VALUES (?,?);";

        $prepStat = $conn->prepare($sql);
        $prepStat->bind_param("ss", $username, md5($password));
        $prepStat->execute();
        $prepStat->close();
        $conn->close();
        
        header("Location: login.php");
        exit();
    }
    else {
        if($_SERVER['REQUEST_METHOD'] == 'POST')
        echo "<br/>All field are required!";
    }
?>

<html>
<head>
    <link rel="stylesheet" media="screen" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/css/bootstrap.min.css" />
    <title>Login Page</title>
</head>
<body style="background-color: <?php echo $_SESSION['color'] ?>">

    <div class="container-fluid">
        <h3>New users registration</h3>
        <br />
        <form name="form1" method="POST" action="register.php">
            <div class="form-group">
                <label for="username">Username: </label>
                <input type='TEXT' name='username' maxlength="20" />
            </div>
            <div class="form-group">
                <label for="password">Password: </label>
                <input type='password' name='password' maxlength="20" />
            </div>
            <input type="Submit" name="Submit1" value="Register" class="btn btn-primary" />
        </form>
        <a href="login.php">Login page</a>
    </div>

    <?php print $errorMessage;?>




</body>
</html>
