<?php session_start();
    if (!(isset($_SESSION['login']) && $_SESSION['login'] != '')) {
        header ("Location: register.php");
    }
    echo $_SESSION['color'];
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" media="screen" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/css/bootstrap.min.css" />
    <title>Homepage</title>

</head>

<body style="background-color: <?php session_start(); echo $_SESSION['color'] ?>">



    <div class="container-fluid" style="margin-top:20px">
        <form class="" action="upload.php" method="post" enctype="multipart/form-data">
            <div class="form-group">
                <label for="fileToUpload">Select text file to upload:</label>
                <input type="file" name="fileToUpload" id="fileToUpload" />
            </div>
            <input type="submit" value="Upload file" name="submit" class="btn btn-warning" />
        </form>
        <br />
        <a href=logout.php>Log out</a>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/js/bootstrap.min.js"></script>
</body>
</html>
