<?php
    if (!isset($_SERVER['PHP_AUTH_USER'])) {
        header("WWW-Authenticate: Basic realm=\"Private Area\"");
        header("HTTP/1.0 401 Unauthorized");
        echo "<h3>You have no permissions to see this page!</h3>";
        exit;
    } else {
        if (($_SERVER['PHP_AUTH_USER'] == 'professor') && ($_SERVER['PHP_AUTH_PW'] == 'pass')) {
            $student = $_POST['student'];
            $grade = $_POST['grade'];

            echo "<b>Student:</b> $student  <br/><b>Grade:</b> $grade";
        } else {
            header("WWW-Authenticate: Basic realm=\"Private Area\"");
            header("HTTP/1.0 401 Unauthorized");
            echo "<h3>You have no permissions to see this page!</h3>";
            exit;
        }
    }
?>

