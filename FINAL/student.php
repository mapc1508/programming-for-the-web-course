<?php
session_start();
if ($_SESSION['loggedIn'] != 1) {
    header ("Location: index.php");
}   
?>

<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" media="screen" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/css/bootstrap.min.css" />

    <title>Student page</title>

</head>

<body>

    <table class="table table-condensed">
        <thead>
            <tr>
                <th>Subject</th>
                <th>Grade</th>
            </tr>
        </thead>
        
        <tbody>

            <?php
                require("db.php");
                $currentStudent = $_SESSION["currentStudent"];
                $SQL = "SELECT * FROM Student_Subject WHERE Student = '$currentStudent' ";

                $result = mysqli_query($conn, $SQL);
                $num_rows = $result->num_rows;
                if ($num_rows > 0) {
                    while($row = $result->fetch_assoc()) {
                        extract($row);
                        echo "<tr>\n";
                        echo "     <td>$Subject</td>\n";
                        echo "     <td>$Grade</td>\n";
                        echo "</tr>\n";
                    }
                }
            ?>
        </tbody>
    </table>

    

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/js/bootstrap.min.js"></script>

    <script></script>
</body>
</html>

