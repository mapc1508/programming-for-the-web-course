<?php 
    session_start();
      if ($_SESSION['loggedIn'] != 1) {
          header ("Location: index.php");
      }
?>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>Add subject</title>

    <!-- Bootstrap -->
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" />

</head>
<body>
    <div class="container-fluid" style="width:75%; margin-left:auto; margin-right:auto; margin-top:20px;">
        <form action="addSubject.php" method="POST" role="form">
            <div class="form-group">
                <label for="subject">Add subject: </label>
                <input type="text" class="form-control" name="subject" />
            </div>
            <input type="submit" value="Add" class="btn btn-success" />
        </form>
        <?php
        require('db.php');
        $name = $_POST['subject'];

        if(!empty($name)){

            $sql = "INSERT INTO Subjects(name) VALUES (?);";

            $prepStat = $conn->prepare($sql);
            $prepStat->bind_param("s", $name);
            $prepStat->execute();
            $prepStat->close();
            $conn->close();

            echo "<br/>New subject has been successfully added!";
        }

        $sql = "SELECT * FROM Users WHERE approved IS NULL ORDER BY username;";
        $result = $conn->query($sql);
        echo "<form action=\"addSubject.php\" method=\"POST\" role=\"form\">\n";
        echo "<div class=\"form-group\">\n";
        echo "<label for='studentName'>Select student:</label>";
        echo "<select name='studentName' class='form-control'>";
        if($result->num_rows > 0){
            for ($i = 0; $i < $result->num_rows; $i++)
            {
                $row = $result->fetch_assoc();
                echo "<option value='" . $row['username'] . "'>" . $row['username'] . "</option>";
            }
        }
        echo "</select>";
        echo "</div>";

        $sql = "SELECT * FROM Subjects ORDER BY name;";
        $result = $conn->query($sql);
        echo "<div class=\"form-group\">\n";
        echo "<label for='subjectName'>Select subject:</label>";
        echo "<select name='subjectName' class='form-control'>";
        if($result->num_rows > 0){
            for ($i = 0; $i < $result->num_rows; $i++)
            {
                $row = $result->fetch_assoc();
                echo "<option value='" . $row['name'] . "'>" . $row['name'] . "</option>";
            }
        }
        echo "</select>";
        echo "</div>";

        echo "	<div class=\"form-group\">\n";
        echo "		<label for=\"grade\">Grade</label>\n";
        echo "		<input type=\"number\" class=\"form-control\" name=\"grade\">\n";
        echo "	</div>\n";


        echo "	<button type=\"submit\" class=\"btn btn-primary\">Submit</button>\n";
        echo "</form>\n";

        echo "<br/><br/>";

        $subject = $_POST['subjectName'];
        $student = $_POST['studentName'];
        $grade = $_POST['grade'];

        if(!empty($subject) && !empty($student)){
                $sql = "INSERT INTO Student_Subject (Student, Subject, Grade) VALUES (?,?,?);";
            $prepStat = $conn->prepare($sql);
            $prepStat->bind_param("ssd", $student, $subject, $grade);
            $prepStat->execute(); 
        }

        $conn->close();
        ?>
    </div>
</body>
</html>
