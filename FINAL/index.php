<?php
$user = "";
$pass = "";
$errorMessage = "";


if ($_SERVER['REQUEST_METHOD'] == 'POST'){
	$user_name = $_POST['username'];
	$pass = $_POST['password'];

	require('db.php');

    $SQL = "SELECT * FROM Users WHERE username = '$user_name' AND pass = '". md5($pass) ."'";

    $result = mysqli_query($conn, $SQL);
    $num_rows = $result->num_rows;

    if ($result) {
        if ($num_rows == 1) {
            $row = $result->fetch_assoc();
            extract($row);
            session_start();
            $_SESSION["loggedIn"] = 1;
            if($approved == 1){
                header ("Location: addSubject.php");
            }
            else if($approved == null){
                $_SESSION["currentStudent"] = $user_name;
                header ("Location: student.php");
            }
            else{
                $errorMessage = "Account is not approved by the administrator";
            }
        }
        else {
            $errorMessage = "No such username and password";
        }
    }
    else {
        $errorMessage = "Code erorr";
    }

    $conn->close();
}

?>

<html>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" media="screen" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/css/bootstrap.min.css" />

    <title>Login Page</title>
</head>

<body>

    <div class="container-fluid">
        <div class="col-md-4 col-md-offset-4">
            <div>
                <form method="POST" action="index.php">
                    <h3>Login</h3>
                    <br />
                    <div class="form-group">
                        <label for="username">Username: </label>
                        <input class="form-control" type='text' name='username' maxlength="20" required />
                    </div>
                    <div class="form-group">
                        <label for="password">Password: </label>
                        <input class="form-control" type='password' name='password' maxlength="20" required />
                    </div>
                    <div class="form-group">
                        <input class="form-control btn btn-primary" type="Submit" value="Login" />
                    </div>
                    <div><a href="register.php">Register</a></div>
                    <div><a href="admin.php">Administration</a></div>
                    <p id="errorMessage">
                        <?php echo $errorMessage; ?>
                    </p>
                </form>
            </div>
        </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.3/jquery.min.js"></script>
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/jquery.validate.min.js"></script>

</body>

</html>
