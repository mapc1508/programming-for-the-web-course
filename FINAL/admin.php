<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" media="screen" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/css/bootstrap.min.css" />

    <title>Admin</title>

</head>

<body>

    <div class="container">


        <?php

// if we are using IIS, we need to set $PHP_AUTH_USER and $PHP_AUTH_PW
if (substr($SERVER_SOFTWARE, 0, 9) == 'Microsoft' &&
    !isset($PHP_AUTH_USER) &&
    !isset($PHP_AUTH_PW) &&
    substr($HTTP_AUTHORIZATION, 0, 6) == 'Basic '
   )
{
    list($_SERVER['PHP_AUTH_USER'], $_SERVER['PHP_AUTH_PW']) = explode(':' , base64_decode(substr($_SERVER['HTTP_AUTHORIZATION'], 6)));
}

// Replace this if statement with a database query or similar
if ($_SERVER['PHP_AUTH_USER'] != 'admin' || $_SERVER['PHP_AUTH_USER'] != 'admin')
{
    // visitor has not yet given details, or their
    // name and password combination are not correct

    header('WWW-Authenticate: Basic realm="Realm-Name"');
    if (substr($SERVER_SOFTWARE, 0, 9) == 'Microsoft')
        header('Status: 401 Unauthorized');
    else
        header('HTTP/1.0 401 Unauthorized');
    echo '<h1>Go Away!</h1>';
    echo 'You are not authorized to view this resource.';
}
else
{
    // visitor has provided correct details


    require('db.php');

    if($_SERVER['REQUEST_METHOD'] === 'POST'){
        $userId = $_POST["userId"];

        $SQL = "UPDATE Users SET approved = 1 WHERE id = $userId ;";
        mysqli_query($conn, $SQL);
    }

    $SQL = "SELECT * FROM Users WHERE approved = 0";

    $result = mysqli_query($conn, $SQL);
    $num_rows = $result->num_rows;

    if ($num_rows > 0) {
        while($row = $result->fetch_assoc()) {
            extract($row);
            echo "<div class=\"panel panel-default\">\n";
            echo "	<div class=\"panel-body\">\n";
            echo "<form action=\"admin.php\" method=\"post\">\n";
            echo "                <input type=\"hidden\" name=\"userId\" value=\"$id\" />\n";
            echo "	  <h4>Professor: $username</h4><input style='float:right' class = 'btn btn-default' type=\"submit\" value=\"Approve\" />\n";
            echo "            </form>";
            echo "</div>";
            echo "</div>";
        }
    }

    $conn->close();

   
}

?>

        </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/js/bootstrap.min.js"></script>

    <script></script>
</body>
</html>
