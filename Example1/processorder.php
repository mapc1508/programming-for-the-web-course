<?php
  // creating short variable names - copy by value
  $tireqty = $_POST['tireqty'];//medium style for getting form fields
  $oilqty = $_POST['oilqty'];
  $sparkqty = $_POST['sparkqty'];
  $find = $_POST['find'];
?>
    <html>

    <head>
        <title>Bob's Auto Parts - Order Results</title>
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/site.css">
        <style>
        .orderBody {
    background-image: url("img/autoparts.jpg");
    background-size: 100% auto;
}
        </style>
    </head>

    <body class="orderBody">
        <div class="container" id="orderDetails">
            <h1>Bob's Auto Parts</h1>
            <h3 style="text-decoration: underline">Order Results</h3>
            <br/>

            <?php
//echo phpinfo();

$totalqty = 0;
$totalqty = $tireqty + $oilqty + $sparkqty;
        
//CONSTANTS
define('TIREPRICE', 100);
define('OILPRICE', 10);
define('SPARKPRICE', 4);

        echo '<h4>Items ordered: '.$totalqty.'</h4>';
echo '<ul class="list-group">';
  

if( $totalqty == 0)
{
    echo "<br /><li class='list-group-item list-group-item-danger'>You did not order anything on the previous page!</li>";
}
else
{
  if ( $tireqty>0 )
    echo "<li class='list-group-item'><b>Tires: </b>$tireqty</li>";
    
  if ( $oilqty>0 )
    echo "<li class='list-group-item'><b>Oil: </b>$oilqty</li>"; 
  if ( $sparkqty>0 )
     echo "<li class='list-group-item'><b>Spark Plugs: </b>$sparkqty</li>";
}

            $date = date('F jS, Y (H:i)');
echo "<li class='list-group-item list-group-item-success'><b>Order processed at: </b>$date</li>";
echo '</ul>';

$totalamount = $tireqty * TIREPRICE
             + $oilqty * OILPRICE
             + $sparkqty * SPARKPRICE;

echo '<b>Subtotal:</b> $'.number_format($totalamount,2).'<br/>';

$taxrate = 0.10;  // local tax for total amount
$totalamount = $totalamount * (1 + $taxrate);
echo '<b>Total including tax:</b> $'.number_format($totalamount,2).'<br/><hr/>';


if($find == 'a' || $find == 'b' || $find == 'c' || $find == 'd'){
    echo '<p><i>Customer referred by ';
    if($find == 'a')
      echo 'Regular customer.</i></p>';
    elseif($find == 'b')
      echo 'TV advert.</p>';
    elseif($find == 'c')
      echo 'phone directory.</i></p>';
    elseif($find == 'd')
      echo 'word of mouth.</i></p>';
}

else
  echo '<p><i>We do not know how this customer found us.</i></p>';


?>
        </div>
    </body>

    </html>