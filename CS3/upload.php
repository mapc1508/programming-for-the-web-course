<?php
$target_dir = "uploads/";
$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
$uploadOk = 1;
$fileType = pathinfo($target_file, PATHINFO_EXTENSION);

// Check if file already exists
if (file_exists($target_file)) {
    echo "File already exists.";
    $uploadOk = 0;
}

// Allow certain file formats
if($fileType != "txt" && $fileType != "doc" && $fileType != "docx") {
    echo "Only txt, doc & docx text files are allowed.";
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "Sorry, your file was not uploaded.";
    // if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
        echo "<h4>The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.</h4>";
        echo "<h4>File type: " . $_FILES["fileToUpload"]["type"] . "</h4>";
        echo "<h4>Path: " . $target_file . "</h4>";
        echo "<a href='$target_file'>Download</a>";

    } else {
        echo "Sorry, there was an error uploading your file.";
    }
}
?>