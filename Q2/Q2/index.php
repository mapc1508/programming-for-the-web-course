<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>AddStudentsGrades</title>

    <!-- Bootstrap -->
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" />

</head>
<body>
    <div class="container-fluid" style="width:50%; margin-left:auto; margin-right:auto; margin-top:20px;">
        <form action="addGrade.php" method="POST" role="form">
            <div class="form-group">
                <label for="student">Add student: </label>
                <input type="text" class="form-control" name="student" />
            </div>
            <div class="form-group">
                <label for="grade">Add grade: </label>
                <input type="number" class="form-control" name="grade" />
            </div>
            <input type="submit" value="Add" class="btn btn-success" />
        </form>
       
    </div>
</body>
</html>
